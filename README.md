Space DAO STM Public UI
=======================

The Space DAO Space Traffic Management (STM) public user interface is made to
show anyone what is happening in terms of space traffic in orbit as well as show
the live state of interactions between stakeholders in orbit when it comes,
mainly, to manage conjunctions in Earth orbit.

Space traffic conjunctions are one of the most prominent issue in orbit. We
cannot afford a collision in orbit. With more than 55000 tracked objects and
with about 7000 to 9000 of these being really operational, a collision would
create debris that are very probably going to hit other assets and create more.

Large collisions happen very rarely, nevertheless, the risk is augmenting
at every launch (weekly 500 to 100 assets are reaching our Earth orbit).
In combinatorial manners, it becomes harder and harder to predict when
collisions are going to happen and the major problem operators encounter now is:
**uncertainty**.

Space DAO offers a verification market for insight concerning space situational
awareness (SSA). It providers operators with consensus that is not decided by a
central authority, preventing from any eventual geo-economical and political problems.
For this we use blockchain (proof of stake ones).

Companies providing SSA information can join the network to reinforce the
consensus and this allows them to access market shares of a currently highly
dominated/locked market.


The STM Public User Interface
------------------------------

This UI should show:
 - a comprehensive menu to access current spacedao.ai content
 - a 3D globe view of conjunctions, not yet another space traffic interface, we
   chose to navigate conjunction alerts (CDMs, Conjunction Data Messages)
 - Textual/Visual information with basic information and statistics about all
   the CDMs and the one CDM selected from the 3D globe
 - Textual/Visual information about blockchain/web3 activities concerning the
   space traffic business exchanges happening live. With a switch that can
   connect betweem the test or main networks.


For developers
--------------

Build and live view:

```bash
npm install
npm run dev
```
